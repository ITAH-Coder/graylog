#!/usr/bin/env bash

cat <<"EOF" > /home/ec2-user/.ssh/authorized_keys
# main infra admin
${ssh_keypair_id_rsa_pub}
# admin team
ssh-rsa xxx
EOF
