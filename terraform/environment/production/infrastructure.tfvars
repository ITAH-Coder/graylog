## --- DEVELOP -----------
environment = "production"
project_name = "iac-infrastructure-graylog"

vpc_id = "vpc-xxx"
ec2_private_ips = [ "x.x.x.x" ]
ec2_subnet_ids = [ "subnet-xxx" ]
graylog_secret_arn = "arn:-Graylog-pxxx"
hostname = "graylog"
azs = [ "eu-central-1a" ]
instance_ami = "ami-xxx"
instance_type = "c4.xlarge"
root_volume_size = "2048"
ec2_ingres_rules = [
    {
      from_port   = 22
      to_port     = 22
      protocol    = "tcp"
      cidr_blocks = "x.x.x.x/24"
      description = "Allow incomming SSH traffic from the Admin VPC"
    },
    {
      from_port   = 5100
      to_port     = 5150
      protocol    = "tcp"
      cidr_blocks = "x.x.x.x/16"
      description = "Allow incomming log traffic from the Admin VPC"
    },
    {
      from_port   = 80
      to_port     = 80
      protocol    = "tcp"
      cidr_blocks = "0.0.0.0/0"
      description = "Allow incomming access console"
    },
    {
      from_port   = 9100
      to_port     = 9100
      protocol    = "tcp"
      cidr_blocks = "0.0.0.0/0"
      description = "Allow incomming access console"
    },
    {
      from_port   = 9200
      to_port     = 9200
      protocol    = "tcp"
      cidr_blocks = "0.0.0.0/0"
      description = "Allow incomming access console"
    },
    {
      protocol    = "icmp"
      from_port   = 8
      to_port     = 0
      cidr_blocks = "0.0.0.0/0"
      description = "Allow ICMP (ping) echo request"
    }
]

ec2_egress_rules = [

    {
      from_port   = 0
      to_port     = 0
      protocol    = "-1"
      cidr_blocks = "0.0.0.0/0"
      description = "Allow outgoing connections to the Internet"
    }
]
