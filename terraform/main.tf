
#    Central LOG - GrayLog

# introduction of Local Values configuration language feature
terraform {
  required_version = ">= 0.10.3"
}

locals {
  tags = "${merge(map("Region", "${var.aws_region}"), map("Environment", "${var.environment}"), var.default_tags)}"
}

# --- / Helpers ---------------------------------------------

module "graylog_secret" {
  source     = "git::https://example-codecommit.eu-central-1.amazonaws.com/v1/repos/secretsmanager_secret.tfmodule"
  secret_arn = "${var.graylog_secret_arn}"
}

module "ssh_admin_keypair" {
  source     = "git::https://example-codecommit.eu-central-1.amazonaws.com/v1/repos/secretsmanager_secret.tfmodule"
  secret_arn = "${var.ssh_keypair_arn}"
}

data "template_file" "authorized_keys" {
  template = "${file("templates/authorized_keys.sh.tpl")}"
  vars {
    ssh_keypair_id_rsa_pub = "${base64decode(lookup(module.ssh_admin_keypair.secret, "public"))}"
  }
}

# --- / S3 Bucket for logs ----------------------------------

resource "aws_s3_bucket" "logs" {
  bucket = "iac-graylog-logs-agn"
  acl    = "private"

  tags = "${merge(map("Project", "${var.project_name}"), local.tags)}"
}



# --- Rsyslog server ----------------------------------
module "appserver" {
  source = "git::https://example-codecommit.eu-central-1.amazonaws.com/v1/repos/ec2instance.tfmodule"

  vpc_id = "${var.vpc_id}"
  private_ips = ["${var.ec2_private_ips}"]
  subnet_ids  = ["${var.ec2_subnet_ids}"]
  hostname    = "${var.hostname}"
  azs         = "${var.azs}"
  project     = "${var.project_name}"
  environment = "${var.environment}"
  zone_id     = "${data.aws_route53_zone.aws_example_eu.zone_id}"
  ami                   = "${var.instance_ami}"
  instance_type         = "${var.instance_type}"
  root_volume_size      = "${var.root_volume_size}"

  # https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/EBSVolumeTypes.html
  root_volume_type = "gp2"
  user_data        = "${data.template_file.authorized_keys.rendered}"

  ingres_rules = "${var.ec2_ingres_rules}"
  egress_rules = "${var.ec2_egress_rules}"

  tags = "${merge(map("Project", "${var.project_name}"), map("AnsibleID", "${var.hostname}-server"), local.tags)}"
}


########################
#     Load Balancer
########################

### Security Group
resource "aws_security_group" "SG-LB-Graylog" {
  depends_on = ["module.appserver"]
  description = "allow_http_https"
  vpc_id      = "${var.vpc_id}"
  name        = "SG-LB-Graylog"
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port       = 0
    to_port         = 0
    protocol        = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}
# END

###  Load Balancer
resource "aws_lb" "LB-Graylog" {
  name               = "LB-Graylog"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.SG-LB-Graylog.id}"]
  subnets            = ["${var.lb_subnets}"]
  tags = {
    Environment = "production"
  }
}

resource "aws_lb_target_group" "TG-Graylog" {
  name        = "TG-Graylog"
  port        = 80
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = "${var.vpc_id}"
}

resource "aws_lb_listener" "LB-Graylog-listener" {
  load_balancer_arn = "${aws_lb.LB-Graylog.arn}"
  port              = "443"
  protocol          = "HTTPS"
  ssl_policy        = "ELBSecurityPolicy-2016-08"
  certificate_arn   = "arn:aws:xxx"

  default_action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.TG-Graylog.arn}"
  }
}

resource "aws_lb_target_group_attachment" "lb-attachment-graylog" {
  target_group_arn = "${aws_lb_target_group.TG-Graylog.arn}"
  target_id        = "${module.appserver.private_ips[0]}"
  port             = 80
}

########################
#     Route53
########################

data "aws_route53_zone" "aws_example_eu" {
  name         = "aws.example.eu"
  private_zone = false
}

resource "aws_route53_record" "graylog" {
  zone_id = "${data.aws_route53_zone.aws_example_eu.zone_id}"
  name    = "graylog.aws.example.eu"
  type    = "A"
  alias {
    name                   = "${aws_lb.LB-Graylog.dns_name}"
    zone_id                = "${aws_lb.LB-Graylog.zone_id}"
    evaluate_target_health = true
  }
}

resource "aws_route53_record" "CNAME" {
  depends_on = ["aws_lb.LB-Graylog"]
  zone_id = "${data.aws_route53_zone.aws_example_eu.zone_id}"
  name    = "_xxx.graylog.aws.example.eu."
  type    = "CNAME"
  records = ["_xxx.acm-validations.aws."]
  ttl     = "300"
}
