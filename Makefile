SHELL := /bin/bash

init:
	cd terraform/ && export TF_VAR_environment=${ENVIRONMENT} && terraform init -no-color -backend-config="key=${ENVIRONMENT}/infrastructure.tfstate" -backend-config="dynamodb_table=graylog_terraform_${ENVIRONMENT}_remote_state_lock" -backend=true -force-copy -get=true -lock=true -input=false

plan:
	cd terraform/ && export TF_VAR_environment=${ENVIRONMENT} && terraform state rm module.graylog_secret
	cd terraform/ && export TF_VAR_environment=${ENVIRONMENT} && terraform plan -out=$(PLAN) -no-color -var-file="environment/${ENVIRONMENT}/infrastructure.tfvars"

apply:
	cd terraform/ && export TF_VAR_environment=${ENVIRONMENT} && terraform apply -no-color -var-file="environment/${ENVIRONMENT}/infrastructure.tfvars" --auto-approve -lock=true $(PLAN)

show:
	cd terraform/ && export TF_VAR_environment=${ENVIRONMENT} && terraform show -no-color

destroy:
	cd terraform/ && export TF_VAR_environment=${ENVIRONMENT} && terraform destroy -no-color -lock=true --auto-approve -var-file="environment/${ENVIRONMENT}/infrastructure.tfvars"

run_playbook:
	chmod 0755 ansible/inventory/ec2.py
	source /venv/bin/activate && export EC2_INSTANCE_FILTERS=$(EC2_INSTANCE_FILTERS) && ansible-playbook -i ansible/inventory/ec2.py -u $(SSH_USER) $(PLAYBOOK)
