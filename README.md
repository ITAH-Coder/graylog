# GrayLog

Automatic deployment on AWS cloud using TERRAFORM & ANSIBLE.
Build the following components:
- MongoDB
- ElasticSearch
- Graylog
- Rsyslog Server
- Rsyslog clients
- Inputs, Role, Users via API


<img src="images/graylog.png " alt="drawing" width="600"/>

